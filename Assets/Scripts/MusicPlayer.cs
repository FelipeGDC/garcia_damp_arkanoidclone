﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

    static private MusicPlayer player = null;
	// Use this for initialization
	void Start () {
        if (player == null) {
            player = this;
            GameObject.DontDestroyOnLoad(gameObject);
        } else {
            GameObject.Destroy(gameObject);
        }
    }
}
